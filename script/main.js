var number = ['1','2','3','4','5','6','7','8','9','del','0','C','(',')','.'];
var operation = ['+','-','*','/','%'];
$(document).ready(function() {
	for (var i = 0; i < number.length; i++) {
		$('.calcNumber').append('<div class="calcButton" data-btn="' + number[i] + '">' + number[i] + '</div>');
	};
	for (var i = 0; i < operation.length; i++) {
		$('.calcOper').append('<div class="calcButtonOper" data-btn="' + operation[i] + '">' + operation[i] + '</div>');
	};

	$('.calcBody').on('click', function(data) {
		var dataInput = data.target.getAttribute('data-btn');
		var input = $('#input');

		switch(dataInput){
			case 'C': 
				input.text('');
				break;
			case 'del': 
				var arr = input.text().split('');
				arr.pop();
				input.text(arr.join(''));
				break;
			case '+': 
				input.text(checkOper('+', input));
				break;
			case '-': 
				input.text(checkOper('-', input));
				break;
			case '*': 
				input.text(checkOper('*', input));
				break;
			case '/': 
				input.text(checkOper('/', input));
				break;

			default: 
				input.append(dataInput);
		}
	});
	
	$('.calcEqually').on('click', function() {
		var expression = eval($('#input').text());
		if (expression == 'Infinity' || expression == '-Infinity') $('#result').text('На 0 делить нельзя!');
		else $('#result').text('= ' + expression);		
	})
	
});

function checkOper(oper, obj) {
	var objOper = obj.text().split('');
	for (var i = 0; i < operation.length; i++) {
		if (objOper[objOper.length - 1] == operation[i]){
			objOper[objOper.length - 1] = oper;
			return objOper.join('');
		}
	}
	objOper.push(oper);
	return objOper.join('');
}